/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author samu
 * @param <T>
 */
public class Equacao2Grau<T extends Number> {
    private T a;
    private T b;
    private T c;
    
    private static final String MSG_DE_ERRO = "Coeficiente a não pode ser zero";
    private static final String MSG_DE_ERRO2 = "Equação não tem solução real";
    
   
    
    //Construtor que recebe os coeficientes da equação quadrática.
    public Equacao2Grau(T a, T b, T c){
        this.a = a;
        this.b = b;
        this.c = c;
        
        if(a.intValue() == 0){
            throw new RuntimeException(MSG_DE_ERRO);
        }
        
    }

    public T getA() {
        return a;
    }

    public void setA(T a) {
        if(a.intValue() == 0){
            throw new RuntimeException(MSG_DE_ERRO);
        }
        this.a = a;
            
    }

    public T getB() {
        return b;
    }

    public void setB(T b) {
        this.b = b;
    }

    public T getC() {
        return c;
    }

    public void setC(T c) {
        this.c = c;
    }
    
    public double getRaiz1(){
        double c1 = getA().doubleValue();
        double c2 = getB().doubleValue();
        double c3 = getC().doubleValue();
        
        double delta = (c2*c2 - 4*c1*c3);
        if(delta < 0){
            throw new RuntimeException(MSG_DE_ERRO2);
        }
        return ((c2 + Math.sqrt(delta))/2*c1);
    }
    
     public double getRaiz2(){
        double c1 = getA().doubleValue();
        double c2 = getB().doubleValue();
        double c3 = getC().doubleValue();
        
        double delta = (c2*c2 - 4*c1*c3);
        if(delta < 0){
            throw new RuntimeException(MSG_DE_ERRO2);
        }
        return ((c2 - Math.sqrt(delta))/2*c1);
    }
    
        
        
}

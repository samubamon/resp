
import utfpr.ct.dainf.if62c.pratica.Equacao2Grau;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica52 {
    public static void main(String[] args) {
       
        Equacao2Grau<Integer> raiz = new Equacao2Grau<>(1,2,-3);
        
        
        System.out.println(raiz.getRaiz1());
        System.out.println(raiz.getRaiz2());
       
       
        
    }
}
